export class Product {
  constructor(
    public id: number,
    public name: string,
    public imageUrl: string,
    public category: string,
    public imageByCategoryUrl: string,
    public price: number,
    public size: any,
    public tagNew: boolean,
    public tagBestSeller: boolean,
    public tagSale: boolean) { }
}


/*
export interface Product {
    id: number;
    name: string;
    imageUrl: string;
    category: string;
    imageByCategoryUrl: string;
    price: number;
    size: any;
    tagNew: boolean;
    tagBestSeller: boolean;
    tagSale: boolean;
}
*/
