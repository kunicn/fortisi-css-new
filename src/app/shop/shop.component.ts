import { Component, Input } from '@angular/core';
import { Product } from './product';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, style, animate, transition } from '@angular/animations';
import {CartService, PRODUCTS} from '../services/cart.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],

  animations: [

    /* FADE IN ANIMATION */
    trigger('fadeInAnimation', [
      state('void', style({
        opacity: 0
      })),
      state('*', style({
        opacity: 1
      })),
      transition('void <=> *', animate(1000))
    ]),

    /*HOVER BORDER ANIMATION */
    trigger('hoverBorder', [
      state('normal', style({
        'border-color': 'transparent'
      })),
      state('marked', style({
        'border-color': '#6c854b'
      })),
      transition('normal <=> marked', animate(300))
    ])
  ]
})
export class ShopComponent {

  @Input() isHomePage: boolean;
  @Input() isShopPage: boolean;
  @Input() hasTag: boolean;
  @Input() tagNewLabel: string;

  /* MODAL */
  closeResult: string;

  products: Product[] = PRODUCTS;

  /* FILTERED CATEGORIES OF PRODUCTS */
  livingRoomProducts = this.products.filter((product) => product.category === 'living-room');
  bedRoomProducts = this.products.filter((product) => product.category === 'bedroom');
  kitchenProducts = this.products.filter((product) => product.category === 'kitchen');
  bathRoomProducts = this.products.filter((product) => product.category === 'bathroom');

  activeProduct: Product;

  /* ANIMATION */
  currentState: string;

  /* TABS*/
  active = 1;

  constructor(private modalService: NgbModal, private cartService: CartService) {}

  /* ANIMATION */
  animate() {
    this.currentState === 'normal' ? this.currentState = 'marked' : this.currentState = 'normal';
  }

  /* MODAL */
  open(content, product) {
    this.activeProduct = product;

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.activeProduct = null;
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  addToCart(product: Product) {
    this.cartService.addToCart(product);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
