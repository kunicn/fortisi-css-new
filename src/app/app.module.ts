import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomePageComponent } from './home-page/home-page.component';
import { HomePageHeaderComponent } from './home-page/components/home-page-header/home-page-header.component';
import { HomePageDescriptionComponent } from './home-page/components/home-page-description/home-page-description.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { AboutPageDescriptionComponent } from './about-page/components/about-page-description/about-page-description.component';
import { ShopPageComponent } from './shop-page/shop-page.component';
import { ShopComponent } from './shop/shop.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { ContactComponent } from './contact/contact.component';
import { MapComponent } from './map/map.component';
import { FooterComponent } from './footer/footer.component';
import { SocialShareComponent } from './social-share/social-share.component';
import { ContactFormComponent } from './contact/contact-form/contact-form.component';
import { NewsletterFormComponent } from './contact/newsletter-form/newsletter-form.component';
import {CartService} from './services/cart.service';
import { CartPageComponent } from './cart-page/cart-page.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomePageComponent,
    HomePageHeaderComponent,
    HomePageDescriptionComponent,
    AboutPageComponent,
    AboutPageDescriptionComponent,
    ShopPageComponent,
    ShopComponent,
    ContactPageComponent,
    ContactComponent,
    MapComponent,
    SocialShareComponent,
    FooterComponent,
    ContactFormComponent,
    NewsletterFormComponent,
    CartPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    CartService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
