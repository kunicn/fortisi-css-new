import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutPageDescriptionComponent } from './about-page-description.component';

describe('AboutPageDescriptionComponent', () => {
  let component: AboutPageDescriptionComponent;
  let fixture: ComponentFixture<AboutPageDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutPageDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutPageDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
