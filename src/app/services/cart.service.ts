import {Injectable} from '@angular/core';
import {Product} from '../shop/product';

/* DATA */
export const PRODUCTS: Product[] = [
  new Product(
    1,
    'Night table',
    'assets/images/shop/featured/night-table.jpg',
    'bedroom',
    'assets/images/shop/categories/bedroom-night-table.jpg',
    9500,
    ['small', 'medium', 'large'],
    true,
    false,
    false
  ),
  new Product(
    2,
    'Table',
    'assets/images/shop/featured/table.jpg',
    'living-room',
    'assets/images/shop/categories/living-room-table.jpg',
    40000,
    ['small', 'medium', 'large'],
    false,
    false,
    false
  ),
  new Product(
    3,
    'Sofa',
    'assets/images/shop/featured/sofa.jpg',
    'living-room',
    'assets/images/shop/categories/living-room-sofa.jpg',
    40000,
    ['small', 'medium', 'large'],
    false,
    false,
    true
  ),
  new Product(
    4,
    'Chair',
    'assets/images/shop/featured/chair.jpg',
    'living-room',
    'assets/images/shop/categories/living-room-chair.jpg',
    5000,
    ['small', 'medium', 'large'],
    false,
    false,
    false
  ),
  new Product(
    5,
    'Kitchen',
    'assets/images/shop/featured/kitchen.jpg',
    'kitchen',
    'assets/images/shop/categories/kitchen-kitchen.jpg',
    89000,
    ['small', 'medium', 'large'],
    false,
    true,
    false
  ),
  new Product(
    6,
    'Bedroom',
    'assets/images/shop/featured/bedroom.jpg',
    'bedroom',
    'assets/images/shop/categories/bedroom-bedroom.jpg',
    350000,
    ['small', 'medium', 'large'],
    false,
    false,
    false
  ),
  new Product(
    7,
    'Armchair',
    'assets/images/shop/featured/armchair.jpg',
    'living-room',
    'assets/images/shop/categories/living-room-armchair.jpg',
    9000,
    ['small', 'medium', 'large'],
    false,
    false,
    false
  ),
  new Product(
    8,
    'Bar stool',
    'assets/images/shop/featured/bar-stool.jpg',
    'kitchen',
    'assets/images/shop/categories/kitchen-bar-stool.jpg',
    3000,
    ['small', 'medium', 'large'],
    false,
    false,
    false
  ),
  new Product(
    9,
    'Cosy couch',
    'assets/images/shop/featured/cosy-couch.jpg',
    'living-room',
    'assets/images/shop/categories/living-room-cosy-couch.jpg',
    350000,
    ['small', 'medium', 'large'],
    false,
    false,
    false
  ),
  new Product(
    10,
    'Dining table',
    'assets/images/shop/featured/dining-table.jpg',
    'kitchen',
    'assets/images/shop/categories/kitchen-dining-table.jpg',
    50000,
    ['small', 'medium', 'large'],
    false,
    false,
    false
  ),
  new Product(
    11,
    'Pillow',
    'assets/images/shop/featured/pillow.jpg',
    'living-room',
    'assets/images/shop/categories/living-room-pillow.jpg',
    1125,
    ['small', 'medium', 'large'],
    false,
    false,
    false
  ),
];

@Injectable()
export class CartService {
  private cartProducts: Product[] = [];

  addToCart(product: Product) {
    this.cartProducts.push(product);
  }

  getCartProducts(): Product[] {
    return this.cartProducts;
  }
}
