import { Component, OnInit } from '@angular/core';
import {CartService} from '../services/cart.service';
import {Product} from '../shop/product';

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.css']
})
export class CartPageComponent {
  cartProducts: Product[];

  constructor(private cartService: CartService) {
    this.cartProducts = cartService.getCartProducts();
  }
}
